package de.tobi.Devotion_Ranking;

import de.tobi.Devotion_Ranking.DVLeague.Riot_APPI;
import de.tobi.Devotion_Ranking.TS.teamspeak_bot;

/**
 * Hello world!
 *
 */
public class App 
{
	
	public static teamspeak_bot ts;
	public static Riot_APPI ritoapi;
	protected static boolean isStarted;
	public static String version = "1.0.7";
    public static void main( String[] args )
    {
    	// Consolen log ausgabe nur warnen ( Teamspeak bot benutz die API slf4j für logs )
    	System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "warn");
    	System.out.println("Starting Devotion Service Version " + version);
    	// Start methode wird gestartet
    	Start();
    }
    
    
    public static void Start() {
    	// Wenn es schon gestartet ist wird es nicht nochmal ausgefürht
    	if(isStarted == false ) {
    		// Ts bot wird erstellt
	    	ts = new teamspeak_bot();
	    	// Bot wird gestartet
	    	ts.StartBot();
	    	// Riot api wird erstellt
	    	ritoapi = new Riot_APPI();
	    	isStarted = true;
	    	// Verbindung zum MySQL server wird aufgebaut
	    	// Consolen ausgabe dass der Gestartet werden konnte
	    	System.out.println("GESTARTET");
    	} else {
    		System.out.println("ERROR - Ist schon gestarted");
    	}
    }
    
    public  void close() {
    	// Nur wennn es gestartet wurde
    	if(isStarted == true) {
    		// Teamspeak wird geclosed
	    	ts.close();
	    	//Consolen ausgabe
	    	System.out.println("GESTOPPT");
	    	isStarted = false;
    	} else {
    		System.out.println("ERROR - Ist nicht gestarted");
    	}
    }
    
}
