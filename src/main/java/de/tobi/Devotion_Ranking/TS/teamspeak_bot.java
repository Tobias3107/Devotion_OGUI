package de.tobi.Devotion_Ranking.TS;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.PrivilegeKeyUsedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;

import de.tobi.Devotion_Ranking.App;
import de.tobi.Devotion_Ranking.DVLeague.DVElo;
import de.tobi.Devotion_Ranking.DVLeague.DVLanes;
import de.tobi.Devotion_Ranking.MySQL.WebsiteSQL;

public class teamspeak_bot {
	// Devo Server
	private String apiNickname = "DV Bot";
	private String host;
	private String username;
	private String pw;
	private int queryport;
	private int port;
	private TS3Config config;
	private TS3Query query;
	protected TS3Api api;
	protected ArrayList<Integer> uidcheck;
	protected ArrayList<Integer> supportChannel;
	protected TimerTask tt;
	protected Timer t;
	protected int NormalRank;
	protected int GuestRank;
	protected int[] AdminRank;
	protected ArrayList<Integer> AdminsOnlineArray;
	protected String AdminOnline;
	protected int AdminOn;
	protected HashMap<String, String> RankIsNullUser;
	protected ArrayList<String> RankNullList;
	protected SimpleDateFormat sdf;
	private WebsiteSQL wsql;

	public teamspeak_bot() {
		apiNickname = "DV Bot";
		AdminOnline = "";
		// Test Server
//		host = "85.14.242.241";
//		username = "QUATSCH";
//		pw = "RXXpWByC";
//		queryport = 10011;
//		port = 9050;
		//Haupt Server
		host = "79.133.47.2";
		username = "Query";
		pw = "RW6iNCxR";
		queryport = 50067;
		port = 5115;
		
		
		uidcheck = new ArrayList<>();
		supportChannel = new ArrayList<>();
		AdminRank = new int[5];
		AdminsOnlineArray = new ArrayList<>();
		RankIsNullUser = new HashMap<>();
		RankNullList = new ArrayList<>();
		sdf = new SimpleDateFormat("HH:mm:ss");
		
		
		DVElo.BRONZE.setTsID(9581); // Bronze 146
		DVElo.SILVER.setTsID(9582); // Silver 147
		DVElo.GOLD.setTsID(9583); // Gold 148
		DVElo.PLATIN.setTsID(9584); // Platin 149
		DVElo.DIAMOND.setTsID(9585); // Diamond 150
		DVElo.MASTER.setTsID(9586); // Master 151
		DVElo.CHALLANGER.setTsID(9587); // Challanger 152
		DVElo.UNRANKED.setTsID(9599); // Unranked 153
		 
		DVLanes.Top.setTSID(9588); // Top
		DVLanes.Jungle.setTSID(9589); // Jungle
		DVLanes.Mid.setTSID(9590); // Mid
		DVLanes.Adc.setTSID(9591); // Adc
		DVLanes.Support.setTSID(9592); // Support
		DVLanes.Fill.setTSID(9593); // Fill
		
		// Support Channel IDS 
		supportChannel.add(25000); 
		supportChannel.add(24998);
		supportChannel.add(24999);
		supportChannel.add(25001);
		supportChannel.add(25002);
		supportChannel.add(25003);
		 
		// Wichtige Ranks
		NormalRank = 9597;
		GuestRank = 9573;
		AdminRank[0] = 9574;
		AdminRank[1] = 9607;
		AdminRank[2] = 9608;
		AdminRank[3] = 9571;
		AdminRank[4] = 9609;
		
//		DVElo.BRONZE.setTsID(9); // Bronze 146
//		DVElo.SILVER.setTsID(10); // Silver 147
//		DVElo.GOLD.setTsID(11); // Gold 148
//		DVElo.PLATIN.setTsID(12); // Platin 149
//		DVElo.DIAMOND.setTsID(13); // Diamond 150
//		DVElo.MASTER.setTsID(14); // Master 151
//		DVElo.CHALLANGER.setTsID(15); // Challanger 152
//		DVElo.UNRANKED.setTsID(25); // Unranked 153
//		DVLanes.Top.setTSID(20); // Top
//		DVLanes.Jungle.setTSID(23); // Jungle
//		DVLanes.Mid.setTSID(22); // Mid
//		DVLanes.Adc.setTSID(21); // Adc
//		DVLanes.Support.setTSID(24); // Support
//		DVLanes.Fill.setTSID(26); // Fill
//		supportChannel.add(2);
//		supportChannel.add(3);
//		supportChannel.add(4);
//		NormalRank = 7;
//		GuestRank = 8;
//		AdminRank[0] = 3;
//		AdminRank[1] = 17;
//		AdminRank[2] = 18;
//		AdminRank[3] = 19;
//		AdminRank[4] = 16;
	}
	
	public void StartBot() {
		//Teamspeak Config wird Erstellt und IP und Query Port wird eingestellt
		config = new TS3Config();
		config.setHost(host);
		config.setQueryPort(queryport);
		
		// ConnectionHandler wird eingestellt -> Bei jedem Disconnect wird die OnDisconnect ausgeführt und bei Jedem Connect wird die onConnect ausgefürht
		config.setConnectionHandler(new ConnectionHandler() {

			@Override
			public void onDisconnect(TS3Query query) {
				// Naricht in die Console, Timer wird geschlossen
				wsql.disconnect();
				System.out.println("TS3 Query Disconnected");
				t.cancel();
				// Admin liste wird gelöscht
				clearAdmin();
			}

			@Override
			public void onConnect(TS3Query qquery) {
				// Der TS bot logt sich ein
				api = qquery.getApi();
				api.login(username, pw);
				// Und Wählt den Virtuallen Server aus
				api.selectVirtualServerByPort(port);
				wsql = new WebsiteSQL("devotion-clan.lima-db.de/", "db_382094_3", "USER382094", "h8i45dkNO937");
				// Name wird geändert
				qquery.getApi().setNickname(apiNickname);
				System.out.println("TS3 Query Connected");
				
				// Listeners werden eingestellt - 
				setListeners();
				// Sicherhaltshalber werden die Admin gecleart und dannach eingetragen.
				clearAdmin();
				checkAllAdmins();
				// Timer Aufgabe wird erstellt
				ArrayList<Integer> PDTD = new ArrayList<>();
				tt = new TimerTask() {

					@Override
					public void run() {
						try {
							// Nachgeguckt ob der Bot noch auf dem Ts 
							if (query.isConnected()) {
								// MySQL check verbindung
									if(wsql != null) {
										if (wsql.isConnected()) {
											// Jeder User auf dem Server wird abgehandelt -> doThings
											
											for (Client c : api.getClients()) {
												if(!PDTD.contains(c.getDatabaseId())) {
													PDTD.add(c.getDatabaseId());
													doThings(c);
													Thread.sleep(200);
												}
											}
											PDTD.clear();
									} else {
										// Reconnect
										System.err.println("MySQL Connection Confused");
										wsql.connect();
									}
								}
							} else {
								// Reconnect
								System.err.println("Query Connection Confused");
								close();
								query = new TS3Query(config);
								return;
							}
						}catch(OutOfMemoryError e) {
							// Im Falle dass der Speicher voll wird dann wird die MySQL verbindung neu aufgebaut
							wsql.disconnect();
							wsql.connect();
						} catch (Exception e) {
							// Im falle eines Unbecannten fehler
							e.printStackTrace();
						}

					}
				};
				

				// Timer wird erstellt
				t = new Timer();
				// Timer wird gestartet mit der Aufgabe oben ( tt )
				t.schedule(tt, 6000, 45000);

			}
		});
		// Die Strategie des Reconnecten.
		config.setReconnectStrategy(ReconnectStrategy.constantBackoff());
		// Error anzeige
		config.setEnableCommunicationsLogging(true);
		// query wird erstellt
		query = new TS3Query(config);
		
		 // Teamspeak Query versucht zu Connecten
		query.connect();
		// Automatisch wird danach der Connectionhandler abgearbeitet
	}

	public void checkAllAdmins() {
		// Jeder user auf dem Server wird die addAdmin methode ausgefürht
		for (Client c : api.getClients()) {
			addAdmin(c);
		}
	}

	// Elo Rank removen wenn er den falschen rank hat
	public void removeRank(ClientInfo c, int g) {
		// alle server gruppen in einen Array (Liste)
		int[] arr = c.getServerGroups();
		// Alles in der Liste wird durchgegangen
		for (int i = 0; i < arr.length; i++) {
			// Alles Gleich mit anderen rank
			// Es wird nachgeguckt ob der in der Liste ausgewählte die gleiche ServerGruppen
			// ID hat wie der in EloInt
			// Wenn ja wird nachgecheckt ob der nicht das gleiche ist wie g. Wenn es nicht
			// gleich ist wird der rank removed wenn nicht dann wird nix gemacht.
			if (arr[i] == DVElo.BRONZE.getTsID() && DVElo.BRONZE.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.BRONZE.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.SILVER.getTsID() && DVElo.SILVER.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.SILVER.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.GOLD.getTsID() && DVElo.GOLD.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.GOLD.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.PLATIN.getTsID() && DVElo.PLATIN.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.PLATIN.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.DIAMOND.getTsID() && DVElo.DIAMOND.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.DIAMOND.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.MASTER.getTsID() && DVElo.MASTER.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.MASTER.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.CHALLANGER.getTsID() && DVElo.CHALLANGER.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.CHALLANGER.getTsID(), c.getDatabaseId());
			}

			if (arr[i] == DVElo.UNRANKED.getTsID() && DVElo.UNRANKED.getTsID() != g) {
				api.removeClientFromServerGroup(DVElo.UNRANKED.getTsID(), c.getDatabaseId());
			}
		}
	}

	// Remove Rank ( Lanes ) Double Client ist für die Untterscheidung für die
	// andere
	public void removeRank(Client c, String g, Client c2) {
		// Gleiche wie oben nur Mit den lanes.
		// Mit einer anderen Technik weil ich die später herausgefunden habe
		if (c.isInServerGroup(DVLanes.Top.getTSID()) && !g.contains("Top")) {
			api.removeClientFromServerGroup(DVLanes.Top.getTSID(), c.getDatabaseId());
		}

		if (c.isInServerGroup(DVLanes.Jungle.getTSID()) && !g.contains("Jungle")) {
			api.removeClientFromServerGroup(DVLanes.Jungle.getTSID(), c.getDatabaseId());
		}

		if (c.isInServerGroup(DVLanes.Mid.getTSID()) && !g.contains("Mid")) {
			api.removeClientFromServerGroup(DVLanes.Mid.getTSID(), c.getDatabaseId());
		}

		if (c.isInServerGroup(DVLanes.Adc.getTSID()) && !g.contains("Adc")) {
			api.removeClientFromServerGroup(DVLanes.Adc.getTSID(), c.getDatabaseId());
		}

		if (c.isInServerGroup(DVLanes.Support.getTSID()) && !g.contains("Sup")) {
			api.removeClientFromServerGroup(DVLanes.Support.getTSID(), c.getDatabaseId());
		}

		if (c.isInServerGroup(DVLanes.Fill.getTSID()) && !g.contains("Fill")) {
			api.removeClientFromServerGroup(DVLanes.Fill.getTSID(), c.getDatabaseId());
		}
	}

	public void setEloRank(String UUID, DVElo Elo) {
		// von der eindeutigen ID zum Client
		ClientInfo client = api.getClientByUId(UUID);
		// Liste für die Server Gruppen. genauso wie bei der removeRank (Elo) nur andere
		// liste
		List<Integer> list = new ArrayList<>();
		for (int i : client.getServerGroups()) {
			list.add(i);
		}
		switch (Elo) {
		// z.b. wenn ELO = BRONZE
		case BRONZE:
			// Alle anderen EloRanks werden removed
			removeRank(client, DVElo.BRONZE.getTsID());
			// wird nachgeschaut ob er den Elo Rank schon hat
			if (!list.contains(DVElo.BRONZE.getTsID())) {
				// wenn nein wird der Rank hinzugefügt
				api.addClientToServerGroup(DVElo.BRONZE.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}

			// Das gleiche mit den anderen Elos.
			break;
		case SILVER:
			removeRank(client, DVElo.SILVER.getTsID());
			if (!list.contains(DVElo.SILVER.getTsID())) {
				api.addClientToServerGroup(DVElo.SILVER.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case GOLD:
			removeRank(client, DVElo.GOLD.getTsID());
			if (!list.contains(DVElo.GOLD.getTsID())) {
				api.addClientToServerGroup(DVElo.GOLD.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case PLATIN:
			removeRank(client, DVElo.PLATIN.getTsID());

			if (!list.contains(DVElo.PLATIN.getTsID())) {
				api.addClientToServerGroup(DVElo.PLATIN.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case DIAMOND:
			removeRank(client, DVElo.DIAMOND.getTsID());
			if (!list.contains(DVElo.DIAMOND.getTsID())) {
				api.addClientToServerGroup(DVElo.DIAMOND.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case MASTER:
			removeRank(client, DVElo.MASTER.getTsID());
			if (!list.contains(DVElo.MASTER.getTsID())) {
				api.addClientToServerGroup(DVElo.MASTER.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case CHALLANGER:
			removeRank(client, DVElo.CHALLANGER.getTsID());
			if (!list.contains(DVElo.CHALLANGER.getTsID())) {
				api.addClientToServerGroup(DVElo.CHALLANGER.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		case UNRANKED:
			removeRank(client, DVElo.UNRANKED.getTsID());
			if (!list.contains(DVElo.UNRANKED.getTsID())) {
				api.addClientToServerGroup(DVElo.UNRANKED.getTsID(), api.getClientByUId(UUID).getDatabaseId());
			}
			break;
		default:
			break;
		}
	}
	
	public void setLanes(String SS, Client c) {
		// alle lanes werden in eine Array getan
		String[] st = SS.split("\n");
		
		// Alle ranks werden durchgegangen.
		for (String s : st) {
			
			switch (DVLanes.getElo(s)) {
			// Lane wird hinzugefügt
			case Top:
				if (!c.isInServerGroup(DVLanes.Top.getTSID())) {
					api.addClientToServerGroup(DVLanes.Top.getTSID(), c.getDatabaseId());
				}
				break;

			case Jungle:
				if (!c.isInServerGroup(DVLanes.Jungle.getTSID())) {
					api.addClientToServerGroup(DVLanes.Jungle.getTSID(), c.getDatabaseId());
				}
				break;

			case Mid:
				if (!c.isInServerGroup(DVLanes.Mid.getTSID())) {
					api.addClientToServerGroup(DVLanes.Mid.getTSID(), c.getDatabaseId());
				}
				break;

			case Adc:
				if (!c.isInServerGroup(DVLanes.Adc.getTSID())) {
					api.addClientToServerGroup(DVLanes.Adc.getTSID(), c.getDatabaseId());
				}
				break;

			case Support:
				if (!c.isInServerGroup(DVLanes.Support.getTSID())) {
					api.addClientToServerGroup(DVLanes.Support.getTSID(), c.getDatabaseId());
				}
				break;

			case Fill:
				if (!c.isInServerGroup(DVLanes.Fill.getTSID())) {
					api.addClientToServerGroup(DVLanes.Fill.getTSID(), c.getDatabaseId());
				}
				break;

			case Keine:

				break;

			default:

				break;
			}
		}
		// Alle Lanes die nicht in der Liste sind werden entfernt
		removeRank(c, SS, c);
	}
	
	public void setListeners() {
		// Listeners werden registriert ( Habe noch nicht herausgefunden wie es geht
		// ohne alle zu nehmen)
		api.registerAllEvents();
		api.addTS3Listeners(new TS3Listener() {

			public void onTextMessage(TextMessageEvent e) {

			}

			public void onServerEdit(ServerEditedEvent e) {

			}

			public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent e) {

			}

			// Support channels ( Wird ausgeführt wenn der Client den channel switcht oder
			// gemoved wird)
			public void onClientMoved(ClientMovedEvent e) {
				
				// Wird nachgeguckt ob der in den Support channel geht
				if (supportChannel.contains(e.getTargetChannelId())) {
					// Wenn er kein admin ist bekommt er eine Naricht
					System.out.println("Client Joined in Support Channel!");

					// Wenn der nicht 0 ist dann bekommt er eine naricht wenn 0 ist bekommt er die
					// andere naricht
					if (api.getClientInfo(e.getClientId()).isInServerGroup(AdminRank[0]) 
						^ api.getClientInfo(e.getClientId()).isInServerGroup(AdminRank[1]) 
						^ api.getClientInfo(e.getClientId()).isInServerGroup(AdminRank[2])
						^ api.getClientInfo(e.getClientId()).isInServerGroup(AdminRank[3])
						^ api.getClientInfo(e.getClientId()).isInServerGroup(AdminRank[4])) {
					} else {
						// Jeder admin bekommt eine Naricht
						for (Integer c : AdminsOnlineArray) {
							try {
								api.sendPrivateMessage(c,
										"Der User \"" + api.getClientInfo(e.getClientId()).getNickname()
												+ "\" ist gerade im Supportbereich. -> "
												+ api.getChannelInfo(e.getTargetChannelId()).getName());
							} catch (Exception e3) {
								// Wenn der admin Disconnected kommt die Naricht
								if (e3.getMessage().contains("invalid clientID")) {
									System.out.println("Invalid Client ID - ERROR - clientId = " + c);
								}
							}
						}
						
						if (api.getClientInfo(e.getClientId()) != null) {
							// ClientInfo bekommt man nur wenn er auf dem Server ist. Wenn er nicht auf dem
							// Server ist bekommt man null
							if (AdminOn != 0) {

								api.sendPrivateMessage(e.getClientId(),
										"Bitte warte in diesem Channel, im Moment ist/sind " + AdminOn
												+ " Admin(s) online um dir zuhelfen. (" + AdminOnline + ")");
							} else {
								api.sendPrivateMessage(e.getClientId(),
										"Tut uns leid, Immoment sind keine Admins online.");
							}
						}

					}

				}
			}

			// wenn der Client den Server leaved
			public void onClientLeave(ClientLeaveEvent e) {
				// wenn der in der Liste UID Check drin ist ( Die liste wo die reinkommen die
				// nicht eingetragen sind )
				if (uidcheck.contains(e.getClientId())) {
					// Wenn ja wird er removed
					uidcheck.remove((Object) e.getClientId());
				}
				// wenn er ein admin ist dann wird die liste gecleared und dann neu eingetragen
				// (Keine andere möglichkeit gefunden den einzelnen zu löschen)
				if (AdminsOnlineArray.contains(e.getClientId())) {
					clearAdmin();
					checkAllAdmins();
					System.out.println(AdminsOnlineArray.size() + " - " + AdminsOnlineArray.toString());
				}
			

			}

			// Wird ausgefürht wenn jemmand Joined
			public void onClientJoin(ClientJoinEvent e) {
				try {
					Client c = api.getClientInfo(e.getClientId());
					Client c1 = api.getClientByUId(e.getUniqueClientIdentifier());
					c1.isRegularClient();
					
					if (c.isInServerGroup(AdminRank[0]) 
							|| c.isInServerGroup(AdminRank[1]) 
							|| c.isInServerGroup(AdminRank[2]) 
							|| c.isInServerGroup(AdminRank[3]) 
							|| c.isInServerGroup(AdminRank[4]) ) {
						// Wenn er admin ist wird er hinzugefügt
						addAdmin(c);
					}
					
					if(c.isInServerGroup(GuestRank)) {
						api.sendPrivateMessage(e.getClientId(), "Wenn du Mitglied dieser Community werden möchtest begib dich bitte in den Channel \"Mitglied werden\"");
					}
					// Wird doThings ausgeführt
					doThings(c);
				} catch (Exception e3) {
					// Fals der Error kommt invalid ClientID
					// der error kommt wenn der User nur kurz auf dem Server ist und dadurch
					// ClientInfo null ist.
					if (e3.getMessage().contains("invalid clientid")) {
						System.out.println("Invalid ClientID - " + e.getClientNickname());
					} else {
						e3.printStackTrace();
					}
				}

			}

			public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {

			}

			public void onChannelMoved(ChannelMovedEvent e) {

			}

			public void onChannelEdit(ChannelEditedEvent e) {

			}

			public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {

			}

			public void onChannelDeleted(ChannelDeletedEvent e) {

			}

			public void onChannelCreate(ChannelCreateEvent e) {

			}
		});
	}

	// Teamspeak wird geschlossen
	public void close() {
		System.out.println("Teamspeak Query Logout");
		t.cancel();
		api.logout();
		query.exit();
	}

	// doThings
	public void doThings(Client c) {

		// Acktuelle Uhrzeit
		String uhrzeit = sdf.format(new Date());

		// Wird nachgeschaut ob er Registrierzt ist im Forum
		if (wsql.isRegistriert(c.getUniqueIdentifier())) {
			// Username wird herausgefunden
			String Username = wsql.getIGNfromUUID(c.getUniqueIdentifier());
			// Nur wenn der Name nicht ERROR ist und länder als 2 oder Nicht null ist und
			// länger als 2
			if (Username != "ERROR" && Username.length() > 2 || Username != null && Username.length() > 2) {
				// guckt ob sein Rank null ist darf nicht sein. Wenn ja dann evtl Falscher name
				if (App.ritoapi.getRanked(Username) != null) {
					if(RankIsNullUser.containsKey(c.getUniqueIdentifier())) {
						RankIsNullUser.remove(c.getUniqueIdentifier());
						RankNullList.remove(c.getUniqueIdentifier());
					}
					// tut the Rank in eine Variabel
					DVElo rank = App.ritoapi.getRanked(Username);
					// Wird in die console mit Uhrzeit reingeschrieben
					System.out.println(uhrzeit + " - " + "Eingetragen : " + c.getNickname() + " hat den rank " + rank
							+ " erhalten (" + Username + ")");
					// Tut den acktuellen rank in die MySQL datenbank
					wsql.setRankedfromUserName(rank.toString(), Username);
					// Set Elo wird ausgefürht
					setEloRank(c.getUniqueIdentifier(), rank);
					// lanes werden herausgefunden die in der Datenbank eingetragen sind
					String SS = wsql.getLanesByUUID(c.getUniqueIdentifier());
					// Wenn der null ist wird nix passieren
					if (SS != null) {
						setLanes(SS, c);
					}
					// Wenn der user nicht in der Gruppe NormalRank ist wird der Hinzugefügt ->
					// Wunsch von Tom
					if (!c.isInServerGroup(NormalRank)) {
						api.addClientToServerGroup(NormalRank, c.getDatabaseId());
					}

					try {
						// Danach schläft der Aufgaben fahrt 800ms
						Thread.sleep(800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				} else {
					System.err.println("[AUTO-RANKING-SYSTEM] ERROR -> RANK is null -> " + c.getNickname() + " - "
							+ c.getUniqueIdentifier() + " - " + Username);
					if(!RankNullList.contains(c.getUniqueIdentifier())) {
						RankIsNullUser.put(c.getUniqueIdentifier(), c.getNickname() );
						RankNullList.add(c.getUniqueIdentifier());
					}

				}
				// Wenn der Username null ist ist
			} else {
				// wenn er nicht in der Liste ist wird es erst ausgeführt
				if (!uidcheck.contains(c.getId())) {
					// Wird in die Console hingeschrieben
					System.out.println(
							uhrzeit + " - " + "User: " + c.getNickname() + " hat sich nicht im Forum eingetragen");
					// Nachgeguckt ob ClientInfo null ist
					if (api.getClientInfo(c.getId()) != null) {
						// Naricht wird gesendet dass er nicht eingetragen ist
						// Wird in die liste eingetragen
						uidcheck.add(c.getId());

					}
				} else {
					// Wenn er schon eingetragen ist wird bekommt er keine naricht und es wird in
					// die console eingetragen
					System.out.println(uhrzeit + " - " + "Nicht Eingetragen : '" + c.getNickname()
							+ "'");
					// für eventuele Narichten um herauszubekommen wer nicht eingetragen ist
				}

			}
			// wenn er nicht riegestriert ist gleiche verfahren wie oben. ( Username null )
		} else {
			if (!uidcheck.contains(c.getId())) {
				System.out
						.println(uhrzeit + " - " + "User: " + c.getNickname() + " hat sich nicht im Forum eingetragen");
				if (api.getClientInfo(c.getId()) != null) {
					uidcheck.add(c.getId());
				}
			} else {
				System.out.println(
						uhrzeit + " - " + "Nicht Eingetragen : '" + c.getNickname() + "' ");
			}
		}
		
	}

	public synchronized void addAdmin(Client c) {
		// Check ob der in der Admin gruppe ist
		if (c.isInServerGroup(AdminRank[0]) && c.isRegularClient() 
				|| c.isInServerGroup(AdminRank[1]) && c.isRegularClient()
				|| c.isInServerGroup(AdminRank[2]) && c.isRegularClient()
				|| c.isInServerGroup(AdminRank[3]) && c.isRegularClient()
				|| c.isInServerGroup(AdminRank[4]) && c.isRegularClient()) {
			// Wird in die Console eingegeben
			System.out.println("Admin  +" + c.getNickname());
			// Alle leerzeilen werden umgeändert
			String str = c.getNickname().replaceAll(" ", "%20");
			// Die Adminonline liste für die Support channel
			AdminOnline = AdminOnline + "[URL=client://0/" + c.getUniqueIdentifier() + "~" + str + "]" + c.getNickname()
					+ "[/URL]; ";
			// Wird in die Liste der Admins eingetragen
			AdminsOnlineArray.add(c.getId());
			// Zahl wieviele Admins es sind wird ein höher gemacht
			AdminOn++;
		}
	}

	public synchronized void  clearAdmin() {
		// Admins liste für die Support Channel wird leergemacht
		AdminOnline = "";
		// Admin zahl wird auf 0 gesetzt
		AdminOn = 0;
		// Admins liste wird geleert
		AdminsOnlineArray.clear();
		System.out.println(AdminsOnlineArray.size() + " - " + AdminsOnlineArray.toString());
	}
}
