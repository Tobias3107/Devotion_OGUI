package de.tobi.Devotion_Ranking.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WebsiteSQL extends DVMSQL{
	
	private Connection con = null;
	
	public WebsiteSQL(String url, String database, String username, String password) {
		super(url, database, username, password);
		con = getConnected();
	}


	public String getUUIDfromIGN(String Username) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid15` FROM `mybb_userfields` WHERE `fid5` = ?");
			ps.setString(1, Username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getString("fid15");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.err.println("[SQL] SELECT ERROR -> getUUIDfromIGN -- Bitte Kontaktiere sie den Developer [Tobias | TobiKraft]");
			System.err.println("MSG : " + e.getMessage());
		}
		return "ERROR getUUIDfromIGN";
	}
	
	public String getIGNfromUUID(String UUID) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid4` FROM `mybb_userfields` WHERE `fid15` = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getString("fid4");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.err.println("[SQL] SELECT ERROR -> getRankedbyUsername -- Bitte Kontaktiere sie den Developer [Tobias | TobiKraft]");
			System.err.println("MSG : " + e.getMessage());
		}
		return "ERROR";
	}
	
	public boolean isRegistriert(String UUID) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM `mybb_userfields` WHERE `fid15` = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String getRankedbyUsername(String Username) {
		
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid5` FROM `mybb_userfields` WHERE `fid15` = ?");
			ps.setString(1, Username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getString("fid5");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.err.println("[SQL] SELECT ERROR -> getRankedbyUsername -- Bitte Kontaktiere sie den Developer [Tobias | TobiKraft]");
			System.err.println("MSG : " + e.getMessage());
		}
		return "Error";
	}
	
	public String getRankedbyUUID(String UUID) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid5` FROM `mybb_userfields` WHERE `fid15` = ?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getString("fid5");
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			System.err.println("[SQL] SELECT ERROR -> setRankedbyUUID -- Bitte Kontaktiere sie den Developer [Tobias | TobiKraft]");
			System.err.println("MSG : " + e.getMessage());
			
		}
		return "Error";
	}
	
	public String getLanesByUUID(String UUID) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid20` FROM `mybb_userfields` WHERE `fid15`=?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getString("fid20") != null) {
					return rs.getString("fid20");
				} else {
					return "FALSE";
				}
			}
			
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "FALSE";
	}
	
	public String getGeschlechtbyUUID(String UUID) {
		try {
			PreparedStatement ps = con.prepareStatement("SELECT `fid19` FROM `mybb_userfields` WHERE `fid15`=?");
			ps.setString(1, UUID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				return rs.getString("fid19");
			}
			
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "FEHLER";
		
	}
	
	/**
	 * NOTHING
	 * @param rank
	 * @param UUID
	 */
	public void setRankedfromUUID(String rank, String UUID) {
		
	}
	
	
	public boolean setRankedfromUserName(String rank, String Username) {
		try {
			PreparedStatement ps = con.prepareStatement("UPDATE `mybb_userfields` SET `fid5`=? WHERE `fid4` = ?");
			ps.setString(1, rank);
			ps.setString(2, Username);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			System.err.println("[SQL] UPDATE ERROR -> setRankedfromUserName -- Bitte Kontaktiere sie den Developer [Tobias | TobiKraft]");
			System.err.println("MSG : " + e.getMessage());
		}
		return false;
	}
	
}
// UPDATE `mybb_userfields` SET `fid5`='GOLD' WHERE `fid4` = 'Tobikraft'