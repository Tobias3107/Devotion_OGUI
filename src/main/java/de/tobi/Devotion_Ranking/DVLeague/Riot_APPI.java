package de.tobi.Devotion_Ranking.DVLeague;

import com.merakianalytics.orianna.Orianna;
import com.merakianalytics.orianna.types.common.Queue;
import com.merakianalytics.orianna.types.common.Region;
import com.merakianalytics.orianna.types.core.league.LeaguePosition;
import com.merakianalytics.orianna.types.core.summoner.Summoner;

public class Riot_APPI {
	
	private String APIKey = "RGAPI-d65f5ea8-bc5a-44c5-9c0a-3cacb7cd74ac";
	public Riot_APPI() {
		// Api key wird eingetragen
		Orianna.setRiotAPIKey(APIKey);
		// Default region wird eingetragen
		Orianna.setDefaultRegion(Region.EUROPE_WEST);
		// Liste der Ranks
	}
	// Ranked herausfinden vom Summonername
	public DVElo getRanked(String sumname) {
		try {
			// Herausfinden Der Ranks (SoloDouq und Flex 5v5)
			LeaguePosition rsolo = Summoner.named(sumname).get().getLeaguePosition(Queue.RANKED_SOLO_5x5); // SoloDouQ
			LeaguePosition fsolo = Summoner.named(sumname).get().getLeaguePosition(Queue.RANKED_FLEX_SR); // Flex
			// Wenn einer von den Beiden null ist wird der ander ausgefürht
			if(rsolo != null && fsolo != null) {
				// Wird anhand der liste nachgeguckt welcher rank höher ist
				if(DVElo.getElo(rsolo.getTier().toString()).getRanking() < DVElo.getElo(fsolo.getTier().toString()).getRanking()) {
					return DVElo.getElo(fsolo.getTier().toString());
				} else if( DVElo.getElo(fsolo.getTier().toString()).getRanking() <= DVElo.getElo(rsolo.getTier().toString()).getRanking()) {
					return DVElo.getElo(rsolo.getTier().toString());
				} else {
					System.out.println("Name: " + sumname + " hat ein Null als Ranked");
					return null;
				}
				// Wenn Flex null ist wird rsolo als rank ausgegeben
			}  else if(rsolo != null && fsolo == null){
				return DVElo.getElo(rsolo.getTier().toString());
				// Wenn SoloDouQ null ist wird Flex ausgegebn
			} else if(fsolo != null && rsolo == null) {
				return DVElo.getElo(fsolo.getTier().toString());
				// Wenn Beide null sind wird unranked ausgegeben
			} else if(fsolo == null && rsolo == null){
				return DVElo.UNRANKED;
			}
			// Wenn nix zutrifft wird null ausgegeben
			return null;
		} catch(NullPointerException ne) {
			
		} catch (Exception e) {
			// wenn ein fehler kommt unzwar dass der Key nicht mehr gültig ist oder der Falsch ist
			if(e.getMessage().contains("returned \"Forbidden\"")) {
				System.err.println("[RIOT-API] ZUGRIFF VERWEIGERT zu Summoners -> FALSCHER KEY oder KEY ABGELAUFEN");
				System.err.println("[RIOT-API-MSG] " + e.getMessage());
			} else {
				e.printStackTrace();
			}
		}
		// Anonsten null
		return null;
	}
}
