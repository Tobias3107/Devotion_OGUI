package de.tobi.Devotion_Ranking.DVLeague;

public enum DVLanes {
	Top(1),Jungle(2),Mid(3),Support(4),Adc(5),Fill(6),Keine(-1);
	
	private int tsID;
	private DVLanes(int TsID) {
		this.tsID = TsID;
	}
	
	public void setTSID(int TsID) {
		this.tsID = TsID;
	}
	
	public static DVLanes getElo(String str) {
		for(DVLanes dv : DVLanes.values()) {
			if(dv.toString().equals(str)) {
				return dv;
			}
		}
		return null;
	}
	
	public int getTSID() {
		return tsID;
	}
}
