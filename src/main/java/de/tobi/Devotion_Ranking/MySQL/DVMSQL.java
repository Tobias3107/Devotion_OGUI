package de.tobi.Devotion_Ranking.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DVMSQL {
	
//	private static String host;
//	private static String port;
	private String url = null;
	private String database = null;
	private String username = null;
	private String password = null;
	private Connection con = null;
	
	/**
	 * 
	 * @param url  		The IP from the Server
	 * @param database  The Database from the Server
	 * @param username	The Username to connect on the Server
	 * @param password	The Password to connect on the Server
	 * Do connect();
	 */
	public DVMSQL(String url,String database,String username, String password) {
		this.url = url;
		this.database = database;
		this.username = username;
		this.password = password;
		connect();
	}
	
	/**
	 *  Connect to the Server will do when Object created
	 */
	public void connect() {
		if(!isConnected()) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://" +url + database, username, password);
				System.out.println("[MySQL] Die Verbindung wurde hergestellt!");
			} catch (Exception e) {
				
				if (e.getMessage().contains("denied"))
				{
					System.err.println("[MySQL] VERBINDUNGSFEHLER -> Falscher Username oder Falsches Password");
					return;
				} else {
					System.err.println("[MySQL] VERBINDUNGSFEHLER -> Etwas anderes ist Schief gelaufen");
				}
			}
		}
	}
	
	/**
	 *  Disconnect on the Server
	 *  Musst be Connected
	 */
	public void disconnect() {
		if(isConnected()) {
			try {
				con.close();
				con = null;
				System.out.println("[MySQL] Die Verbindung wurde geschlossen!");
			} catch (SQLException e) {
				System.err.println("[MySQL] Error beim schließen der Verbindung");
			}
		}
	}
	/**
	 * Check is Connected
	 * @return True -> is connected
	 * @return False -> Isnt Connected
	 */
	public boolean isConnected() {
		if(con != null) {
			try {
				return !con.isClosed();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			return false;
		}
		return false;
	}
	
	/**
	 * 
	 * @return the Connection from the Server
	 * @return null -> not Connected
	 */
	public Connection getConnected() {
		return con;
	}
}
