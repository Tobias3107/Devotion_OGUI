package de.tobi.Devotion_Ranking.DVLeague;

public enum DVElo {
	UNRANKED(0,0),BRONZE(1,1),SILVER(2,2),GOLD(3,3),PLATIN(4,4),DIAMOND(5,5),MASTER(6,6),CHALLANGER(7,7);
	
	private int tsID;
	private int Ranking;
	private DVElo(int ID,int Ranking) {
		this.tsID = ID;
		this.Ranking = Ranking;
	}
	public void setTsID(int id) {
		this.tsID = id;
	}
	
	public int getTsID() {
		return tsID;
	}
	
	public int getRanking() {
		return Ranking;
	}
	
	public static DVElo getElo(String str) {
		for(DVElo dv : DVElo.values()) {
			if(dv.toString().equals(str)) {
				return dv;
			}
		}
		return null;
	}
}
